const ValidateEmail = (email) => {
  let emailValid = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (email === undefined) {
    return 'ERROR : No Parameter';
  }
  if (typeof email !== 'string') {
    return 'ERROR : Parameter is not a string';
  }
  if (email.indexOf('@') == -1) {
    return 'ERROR : Email did not contain an "@"';
  }

  if (email.match(emailValid)) {
    return 'VALID';
  }
  return 'INVALID';
};

console.log(ValidateEmail('apranata@binar.co.id'));
console.log(ValidateEmail('apranata@binar.com'));
console.log(ValidateEmail('apranata@binar'));
console.log(ValidateEmail('apranata'));
console.log(ValidateEmail(3322));
console.log(ValidateEmail());
