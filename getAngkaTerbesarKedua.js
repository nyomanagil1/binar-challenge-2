const getAngkaTerbesarKedua = (dataNumbers) => {
  if (dataNumbers === undefined) {
    return 'ERROR : Bro where is the parameter?';
  }
  if (typeof dataNumbers === 'object') {
    angkaTerbesar = Math.max(...dataNumbers);
    i = dataNumbers.indexOf(angkaTerbesar);
    dataNumbers.splice(i, 1);
    angkaTerbesarKedua = Math.max(...dataNumbers);
    return angkaTerbesarKedua;
  }
  return 'ERROR : Parameter is not an Array';
};

console.log(getAngkaTerbesarKedua([1, 2, 3, 4, 5, 2, 4, 1, 7, 9, 1, 2, 3]));
console.log(getAngkaTerbesarKedua(0));
console.log(getAngkaTerbesarKedua());
