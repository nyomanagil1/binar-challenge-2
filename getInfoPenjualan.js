const dataPenjualanNovel = [
  {
    idProduct: 'BOOK002421',
    namaProduk: 'Pulang - Pergi',
    penulis: 'Tere Liye',
    hargaBeli: 60000,
    hargaJual: 86000,
    totalTerjual: 150,
    sisaStok: 17,
  },
  {
    idProduct: 'BOOK002351',
    namaProduk: 'Selamat Tinggal',
    penulis: 'Tere Liye',
    hargaBeli: 75000,
    hargaJual: 103000,
    totalTerjual: 171,
    sisaStok: 20,
  },
  {
    idProduct: 'BOOK002941',
    namaProduk: 'Garis Waktu',
    penulis: 'Fiersa Besari',
    hargaBeli: 67000,
    hargaJual: 99000,
    totalTerjual: 213,
    sisaStok: 5,
  },
  {
    idProduct: 'BOOK002941',
    namaProduk: 'Laskar Pelangi',
    penulis: 'Andrea Hirata',
    hargaBeli: 55000,
    hargaJual: 68000,
    totalTerjual: 20,
    sisaStok: 56,
  },
];

const getInfoPenjualan = (dataPenjualan) => {
  let rupiahTotalKeuntungan = 0; //variabel totalKeuntungan
  let rupiahTotalModal = 0; //variabel totalModal

  let bukuTerlaris = dataPenjualan.reduce(function (prev, curr) {
    return curr.totalTerjual > prev.totalTerjual ? curr : prev.namaProduk;
  }); //Buku Terlaris

  for (i = 0; i < dataPenjualan.length; i++) {
    rupiahTotalKeuntungan += dataPenjualan[i].totalTerjual * (dataPenjualan[i].hargaJual - dataPenjualan[i].hargaBeli); //totalKeuntungan

    rupiahTotalModal += dataPenjualan[i].hargaBeli * (dataPenjualan[i].totalTerjual + dataPenjualan[i].sisaStok); //totalModal

    angkaPersentaseKeuntungan = (rupiahTotalKeuntungan / rupiahTotalModal) * 100; //Persentase Keuntungan
  }
  // Membuat data baru yang sudah dijumlah saat ada penulis yang sama
  const newData = [];
  dataPenjualanNovel.forEach((x) => {
    const obj = newData.find((o) => o.penulis === x.penulis);
    if (obj) {
      obj.totalTerjual = obj.totalTerjual + x.totalTerjual;
    } else {
      newData.push(x);
    }
  });
  // End of membuat data baru
  // Reduce data barunya
  let findPenulisTerlaris = newData.reduce(function (prev, curr) {
    return curr.totalTerjual > prev.totalTerjual ? curr.penulis : prev;
  });
  // End of reduce data baru
  // Cari penulis terlaris dari data baru
  let penulisTerlaris = findPenulisTerlaris.penulis;
  // End of penulisTerlaris

  let totalKeuntungan = 'Rp.' + new Intl.NumberFormat('id-ID').format(rupiahTotalKeuntungan); // Format Rupiah
  let totalModal = 'Rp.' + new Intl.NumberFormat('id-ID').format(rupiahTotalModal); //Format Rupiah
  let persentaseKeuntungan = angkaPersentaseKeuntungan.toFixed(2) + '%'; //Memberikan persentase dan memotong angka dibelakang koma
  return { totalKeuntungan, totalModal, persentaseKeuntungan, bukuTerlaris, penulisTerlaris }; //return
};

console.log(getInfoPenjualan(dataPenjualanNovel));
