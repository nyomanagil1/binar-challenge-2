const getSplitName = (personName) => {
  let names = String(personName).split(' ');
  if (typeof personName !== 'string') {
    return 'ERROR : Invalid Parameter, should be a string';
  }
  if (names.length > 3) {
    return 'Error : This function is only for 3 characters name';
  }

  if (names.length < 2) {
    return { firstName: names[0], middleName: null, lastName: null };
  }
  if (names.length == 3) {
    return { firstName: names[0], middleName: names[1], lastName: names[2] };
  }
  return { firstName: names[0], middleName: null, lastName: names[names.length - 1] };
};

console.log(getSplitName('Nyoman Agil Putra'));
console.log(getSplitName('Nyoman Agil'));
console.log(getSplitName('Nyoman'));
console.log(getSplitName('Nyoman Agil Putra Sudiana'));
console.log(getSplitName(0));
